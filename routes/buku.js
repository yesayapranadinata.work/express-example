const express = require('express');

const router = express.Router();

const bukuController = require('../controllers/buku');

const auth = require('../configs/auth');

//mengambil semua yang ada di tabel buku 
router.post('/getAll/', bukuController.listBuku);

//mengambil detail salah satu buku sesuai dengan id nya  
router.get('/find/:id', bukuController.detailBuku);

//menambah kan buku harus seorang admin 
router.post('/postAdd/', auth.verifyToken, bukuController.insertBuku);

//mengupdate buku harus seorang admin  
router.put('/putAdd/:id', auth.verifyToken , bukuController.updateBuku);

//mendelete buku harus seorang admin  
router.delete('/delete/:id', auth.verifyToken , bukuController.deleteBuku);

module.exports = router;