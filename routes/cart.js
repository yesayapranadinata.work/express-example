const express = require('express');

const router = express.Router();

const cartControllers = require('../controllers/cart');
const auth = require('../configs/auth');

//melihat semua data di tabel cart harus login admin
router.post('/getAll', auth.verifyToken , cartControllers.getAll);

//melihat data cart dengan id tertentu harus login admin 
router.post('/findCart/:id', auth.verifyToken , cartControllers.getFindIdCart);

//menambahkan cart harus login user
router.post('/postAdd', auth.verifyToken , cartControllers.postAddCart);

//melihat cart kita sesuai dengan id user nya yang terdapat di table user
router.post('/myCart' , auth.verifyToken , cartControllers.getFindMyCart);


module.exports = router;