const express = require('express');
const bodyParser = require('body-parser');
const sequelize = require('./configs/sequelize');

const app = express();

app.use(bodyParser.json());
app.use(express.static('public'));
app.set('view engine', 'ejs');

const homeRouter = require('./routes/home');
const bukuRouter = require('./routes/buku');
const userRouter = require('./routes/user');
const cartRouter = require('./routes/cart');

const User = require('./models/user');
const Buku = require('./models/buku');
const Cart = require('./models/cart');

app.use(homeRouter);
app.use('/buku', bukuRouter);
app.use('/user', userRouter);
app.use('/cart', cartRouter);

app.listen(3311, () => {
    console.log('server started at port 3311');
    sequelize.sync();
})
