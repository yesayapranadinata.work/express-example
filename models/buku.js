const Sequelize = require('sequelize');

const sequelize = require('../configs/sequelize');

class Buku extends Sequelize.Model {}

Buku.init({
  judul: Sequelize.STRING,
  isbn: Sequelize.STRING,
  type_buku: Sequelize.STRING,
  pengarang: Sequelize.STRING,
  penerbit: Sequelize.STRING,
  tahun_publikasi: Sequelize.DATE,
  harga: Sequelize.FLOAT,
  stok: Sequelize.INTEGER
}, { sequelize, modelName: 'buku' });

module.exports = Buku;