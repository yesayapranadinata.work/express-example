const bcrypt = require('bcryptjs');
const dotenv = require('dotenv');
dotenv.config();
const jwt = require('jsonwebtoken');
const User = require('../models/user');

module.exports.postRegister = (req,res) =>{
    var salt = bcrypt.genSaltSync(10);
    var hash = bcrypt.hashSync(req.body.password, salt);

    User.findOrCreate({
      where:{
          email: req.body.email,
      },
      defaults: {
          username: req.body.username,
          nama_depan: req.body.nama_depan,
          nama_belakang: req.body.nama_belakang,
          email : req.body.email,
          password : hash,
          roles: req.body.roles
      }
    }).then((user) => {
      res.json(user);
    }).catch((error)=>{
      console.log(error);
    });
}


module.exports.postLogin = (req, res) =>{
    User.findOne({
      where: {
        email : req.body.email
      }
    }).then((user) => {
      if(!user) {
        res.status(400).send('username not found');
      }

      bcrypt.compare(req.body.password, user.get('password'),function (err, isMatch){
        if(err) {
          res.status(400).send('Password Error');
        };

        if(isMatch) {
          jwt.sign({ id: user.get('id'),roles:user.get('roles') }, process.env.SECRETKEY, (error,token) =>{
            res.json({token: token});
          })
        } else {
          res.status(400).send('wrong password');
        }
      })
    });
}
