const bcrypt = require('bcryptjs');
const dotenv = require('dotenv');
const jwt = require('jsonwebtoken');
const Buku = require('../models/buku');

dotenv.config();


module.exports.listBuku = (req, res) => {
	Buku.findAll().then((buku) => {
		res.json(buku);
	})
}

module.exports.detailBuku = (req, res) => {
	Buku.findOne({
		where: {
			id: req.params.id
		}
	}).then((buku) => {
		res.json(buku);
	});
}

module.exports.insertBuku = (req, res) =>{
  jwt.verify(req.token, process.env.SECRETKEY, (error,authData)=>{
		if (error) {
			res.sendStatus(403);
		}else{
			if (authData['roles']=="admin") {
				Buku.create({
					judul : req.body.judul,
					isbn : req.body.isbn,
					type_buku : req.body.type_buku,
					pengarang : req.body.pengarang,
					penerbit : req.body.penerbit,
					tahun_publikasi : req.body.tahun_publikasi,
					harga : req.body.harga,
					stok : req.body.stok
				}).then((buku) => {
					res.json(buku);
				}).catch((error) => {
					throw error;
				})
			}
		}
	})
}
  
module.exports.updateBuku = (req, res) =>{
  jwt.verify(req.token, process.env.SECRETKEY, (error,authData)=>{
		if (error) {
			res.sendStatus(403);
		}else{
			if (authData['roles']=="admin") {
				Buku.update({
					judul : req.body.judul,
					isbn : req.body.isbn,
					type_buku : req.body.type_buku,
					pengarang : req.body.pengarang,
					penerbit : req.body.penerbit,
					tahun_publikasi : req.body.tahun_publikasi,
					harga : req.body.harga,
					stok : req.body.stok
				}, {
					where: {
						id: req.params.id
					}
				}).then((buku) => {
					res.json(buku);
				}).catch((error) => {
					throw error;
				})
			}
		}
	}
	)
}

module.exports.deleteBuku = (req, res) => {
  jwt.verify(req.token, process.env.SECRETKEY, (error,authData)=>{
    if(error){
      res.send("Error Bro");
    }else{
      if(authData['roles'] == "admin"){
       Buku.destroy({
        where: {
          id : req.params.id
        }
      }).then((buku)=> {
        res.send('data berhasil dihapus')
      })
    }else{
      res.send("Error Lain admin");
    }
  }
})
}

