const dotenv = require('dotenv');
dotenv.config();
const jwt = require('jsonwebtoken');
const Cart = require('../models/cart');



module.exports.getAll= (req, res) =>{
	jwt.verify(req.token, process.env.SECRETKEY, (error,authData)=>{
		if (error) {
			res.sendStatus(403);
		}else{
			if(authData['roles'] == "admin"){
				Cart.findAll().then(Cart=> {
					res.json(Cart);
				}).catch((error)=>{
					console.log(error);
				});
			}else{
				res.sendStatus(403);
			}
		}
	})
	
}
module.exports.getFindIdCart = (req, res) =>{
	jwt.verify(req.token, process.env.SECRETKEY, (error,authData)=>{
		if(error){
			res.sendStatus(403);
		}else{
			if(authData['roles'] == "admin"){
				Cart.findByPk(req.params.id).then(Cart => {
					res.json(Cart)
				})
			}else{
				res.sendStatus(403);
			}
		}
	})
	
}
module.exports.getFindMyCart = (req, res) =>{
	jwt.verify(req.token, process.env.SECRETKEY, (error,authData)=>{
		if (error) {
			res.sendStatus(403);
		}else{
			if (authData['roles']=="user") {
				Cart.findAll({ where: { userId: authData['id'] } }).then(cart => {
					res.json(cart)
				});
			}else{
				res.send("anda bukan user");
			}
		}
	})
	
}


module.exports.postAddCart = (req, res) =>{
	jwt.verify(req.token, process.env.SECRETKEY, (error,authData)=>{
		if (error) {
			res.sendStatus(403);
		}else{
			if (authData['roles']=="user") {
				var bukuId = req.body.bukuId;
				Cart.create({
					userId: authData['id'],
					bukuId: bukuId
				})
				.then(Cart => {
					res.json(Cart);
				});
			}else{
				res.sendStatus(403);
			}
		}
	});
}